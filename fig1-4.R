options(crayon.enabled = FALSE)
library(tidyverse)
library(starvz)
library(patchwork)
library(viridis)
dist_1d_1d_compute_matrix <- function(s){
   p <- length(s)
   S <- 0
   Sa <- rep(0, p)

   if(p==1){
     M <- matrix(nrow=p, ncol=p)
     M[1, 1] <- 1
     col_sizes <- c(1)
     lin_sizes <- c(1)
     v <- c(1)
     return(list(matrix=M, col_sizes=col_sizes, lin_sizes=lin_sizes, areas=v))
   }

   fp <- matrix(nrow=p, ncol=p)
   fc <- matrix(nrow=p, ncol=p)
   for(q in seq(1, p)){
     S <- S + s[q]
     Sa[q] <- S
     fp[1, q] <- 1 + S * q
     fc[1, q] <- 0
   }

   for(C in seq(2, p)){
     for(q in seq(C, p)){
       min_v <- 999999
       min_i <- -1
       for(r in seq(1, q-C+1)){
         Sx <- 0
         for(i in seq(q-r+1, q) ){
             Sx <- Sx + s[i]
         }
         v <- 1 + Sx * r + fp[C-1, q-r]
         if(v < min_v){
            min_v <- v
            min_i <- r
         }
       }
       fp[C, q] <- min_v
       fc[C, q] <- q - min_i
     }
   }

   min_v <- 999999
   min_i <- -1

   #Find best number of colunms
   for(i in seq(1, p)){
      if(min_v > fp[i, p]){
         min_v <- fp[i, p]
         min_i <- i
      }
   }

   min_i

   q = p
   k = rep(0, p)
   if(min_i>1){
   #Number Lines per Column
     for(y in seq(min_i, 2)){
         k[y] <- q - fc[y, q]
         q = fc[y, q]
     }
   }

   k[1] <- q
   k
   fp
   fc
   #print(k)
   #print(fc)
   lin_sizes <- c()
   col_sizes = rep(0, min_i)
   atual <- 1
   atual2 <- 1
   for(i in seq(1, min_i)){

      col_sum <- 0
      for(y in seq(1, k[i])){
         col_sum <- col_sum + s[atual]
         atual <- atual + 1
      }
      col_sizes[i] <- col_sum
      col_sum <- 0
      for(y in seq(1, k[i])){
         col_sum <- col_sum + s[atual2]
         lin_sizes <- c(lin_sizes, round(col_sum/col_sizes[i], 10))
         atual2 <- atual2 + 1
      }
   }
   #print(lin_sizes)
   lin_sizes <- unique(sort(lin_sizes))
   #print(lin_sizes)
   sum <- 0
   for(z in seq(1, length(lin_sizes))){
      lin_sizes[z] <- lin_sizes[z] - sum
      sum <- sum + lin_sizes[z]
   }

   M <- matrix(ncol=length(col_sizes), nrow=length(lin_sizes))
   tol = 1e-5
   atual <- 1
   for(x in seq(1, length(col_sizes))){
      sum <- 0
      for(y in seq(1, length(lin_sizes))){
         sum <- sum + lin_sizes[y]
         area <- sum * col_sizes[x]
         #print(paste0(s[atual], " - ", x, "x", y, " - ", sum, " - ", area, " - ", atual))
         if( round(s[atual], 5) > round(area, 5)){
            M[y, x] <- atual
         }else{
            sum <- 0
            M[y, x] <- atual
            atual <- atual + 1

         }
      }
      #atual <- atual + 1
   }

   #Check
   v <- rep(0, p)
   for(c in 1:length(col_sizes)){
      for(l in 1:length(lin_sizes)){
         v[M[l,c]] = v[M[l,c]] + lin_sizes[l]*col_sizes[c]
      }
   }
   #v

   return(list(matrix=M, col_sizes=col_sizes, lin_sizes=lin_sizes, areas=v))
}
dist_1d_1d_save_pm2 <- function(CM, line_h=NA, col_h=NA){
   eg <- expand_grid(l = seq(1, length(CM$lin_sizes)), c = seq(1, length(CM$col_sizes)))

   lin0_sizes <- cumsum(c(0, CM$lin_sizes))
   col0_sizes <- cumsum(c(0, CM$col_sizes))
   line_sizes <- cumsum(CM$lin_sizes)
   cole_sizes <- cumsum(CM$col_sizes)

   mini_lines_x0 <- c()
   mini_lines_x1 <- c()
   mini_lines_y0 <- c()
   mini_lines_y1 <- c()

   for(i in seq(1, length(CM$col_sizes))){
      for(y in seq(2, length(CM$lin_sizes))){
         if(CM$matrix[y, i]!=CM$matrix[y-1, i]){
             mini_lines_x0 <- c(mini_lines_x0, col0_sizes[i])
             mini_lines_x1 <- c(mini_lines_x1, col0_sizes[i+1])
             mini_lines_y0 <- c(mini_lines_y0, lin0_sizes[y])
             mini_lines_y1 <- c(mini_lines_y1, lin0_sizes[y])
         }
      }
   }

   mini_lines <- data.frame(x0=mini_lines_x0, x1=mini_lines_x1, y0=mini_lines_y0, y1=mini_lines_y1)

   #print(mini_lines_x0)
   print(y)
   eg %>% group_by(l, c) %>% mutate(slin = lin0_sizes[l],
                                   elin = line_sizes[l],
                                   scol = col0_sizes[c],
                                   ecol = cole_sizes[c],
                                   mlin = (elin-slin)/2 + slin,
                                   mcol = (ecol-scol)/2 + scol,
                                   m = CM$matrix[l, c]
                                   ) %>%
       mutate(Case = (c %in% col_h | l %in% line_h)) %>%
       mutate(Node = as.integer(as.character(m))-1) -> df

   df %>% ungroup() %>%  group_by(Node, Case) %>%
          summarize(mcol=mean(mcol), mlin=mean(mlin)) -> df_t

   spread = 0.02
   tibble(Y = line_sizes,
          X.min = 0 - spread,
          X.max = 1 + spread,
          Node = 0) -> df.line_sizes

   ggplot(data=NULL) +
       theme_void(base_size=10) +
       geom_rect(data=df,
                 aes(ymin=slin,
                     ymax=elin,
                     xmin=scol,
                     xmax=ecol,
                     fill=Node,
                     alpha=Case), color="black", size=.1, linetype="dotted") +
       #geom_rect(alpha=0.8) +
       
       geom_text(data=df_t,
                 aes(x=mcol, y=mlin, label=Node, alpha=Case), size=6) +
       scale_alpha_discrete(limits=c(FALSE, TRUE), breaks=c(FALSE, TRUE),
                            range=c(0.15,0.6)) +
       geom_vline(xintercept=col0_sizes, size=0.5, alpha=1) +
       geom_hline(yintercept=c(0, 1), size=0.5, alpha=1) +
       geom_segment(data=mini_lines, aes(x=x0, xend=x1, y=y0, yend=y1)) +
       scale_y_reverse(expand=c(0.0015,0.000)) +
       scale_x_continuous(expand=c(0.0015,0.000)) +
       scale_fill_viridis() +
       theme(
           legend.position = "none",
           axis.title = element_blank(),
           axis.text = element_blank(),
           axis.ticks = element_blank() 
       ) +
       xlab("") + ylab("")-> p
   return(p)
}
box_text <- function(xmin, xmax, ymin, ymax, x, y, label, size=5){
  if(is.na(x)){
    x=(xmax-xmin)/2+xmin
  }
  if(is.na(y)){
    y=(ymax-ymin)/2+ymin
  }
  dlabel <- data.frame(label=label, x=x, y=y)
  return(list(annotate("rect", xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax,
            color=alpha(c("black"), 0.8), alpha=0, linetype = "dotted"),
  geom_label(data=dlabel, aes(label=label, y=y, x=x),
             fill = alpha(c("white"), 0.7), size=size)
  )
  )
}
data_example <- starvz_read("traces/chameleon_exps/chameleon_simu_vis_N2GPUL__96000__30/", selective=FALSE)

panel_progress(data_example$Application, 20, 0.01, show_abe=FALSE,
               plot_node_lines=TRUE,
               plot_cluster_info=TRUE) -> all_vis

all_vis$original_metrics +
  box_text(0, 15000, 0, 0.62, NA, 0.50, "A.3") +
  box_text(19000, 25000, 0.67, 0.9, NA, 0.70, "B.3")-> final

ggsave("./imgs/example_metrics.png", plot=final, width=600, height=400, units = "px", dpi=200, scale=3.0)

ny <- theme(
        axis.title.y=element_blank()
        )

nx <- theme(
        axis.title.x=element_blank()
        )

sx <- scale_x_continuous(breaks = scales::pretty_breaks(n = 3)) 
  
all_vis$plot_den[[21]] <- NULL
pcluster <- all_vis$plot_den[[1]] + nx + sx
for(i in seq(2, 10)){
p <- all_vis$plot_den[[i]] + sx
if(i < 16) p <- p + nx
if((i-1) %% 5 != 0) p <- p + ny
pcluster <- pcluster + p
}
(pcluster & xlab("Metric value [0, 1]") & ylab("Density")) + plot_layout(ncol = 5)-> pcluster

ggsave("./imgs/example_kde.png", plot=pcluster, width=600, height=230, units = "px", dpi=200, scale=2.5)

set.seed(5)
panel_progress(data_example$Application, 20, 0.01, show_abe=TRUE) -> all_vis_abe

ggsave("./imgs/example_cluster.png", plot=all_vis_abe$cluster_metrics, width=600, height=350, units = "px", dpi=200, scale=3.5)

data_example$config$expand <- 0
data_example$config$st$labels <- "NODES_only"
data_example$Colors <- data_example$Colors %>%
  mutate(Color = case_when(Value == "dgetrf_nopiv" ~ "#E41A1C",
                           Value == "dplgsy" ~ "#c7c22e",
                           TRUE ~ Color))
panel_st(data_example, agg=TRUE, agg_met="nodes") +
  xlim(0, 40000) -> gantt_example

(gantt_example +
 box_text(1500, 17000, 0, 60, 9000, 30, "A.1") +
 box_text(18000, 39000, 44, 56, NA, NA, "B.1") +
 box_text(18000, 39000, 32, 36, NA, NA, "B.1") +
 box_text(18000, 39000, 20, 24, NA, NA, "B.1") +
 box_text(18000, 39000, 8, 12, NA, NA, "B.1") +
 box_text(18000, 39000, 36, 44, NA, NA, "B.2") +
 box_text(18000, 39000, 24, 32, NA, NA, "B.2") +
 box_text(18000, 39000, 12, 20, NA, NA, "B.2") +
 box_text(18000, 39000, 0, 8, NA, NA, "B.2") 
  )&
  scale_y_continuous(
      breaks = seq(59, 0, by=-2)+0.5,
      labels = seq(0, 59, by=2)/2,
    expand = c(0, 0)
    ) &
  guides(fill = guide_legend(nrow = 1)) -> p
ggsave("./imgs/example_gantt.png", plot=p, width=600, height=600, units = "px", dpi=200, scale=3)
